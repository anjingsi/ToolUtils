package com.anjingsi.tools;

import org.springframework.beans.BeanUtils;

public class ConvertToTarget{

	/**
	 * 实体类之间的转换
	 *
	 * @param entity
	 * @param doClass
	 * @return
	 */
	public static <T> T entityToClass(Object entity, Class<T> doClass) {
		if (entity == null) {
			return null;
		}
		if (doClass == null) {
			return null;
		}
		try {
			T newInstance = doClass.newInstance();
			BeanUtils.copyProperties(entity, newInstance);
			return newInstance;
		} catch (Exception e) {
			return null;
		}
	}
}