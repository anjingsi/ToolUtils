package com.anjingsi.tools.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 *  使用的方法
 *   @Bean
     ExecutorService getExecutorService(){
     return OpsExecutorService.getInstance();
    }
 */
public class ExecutorServiceUtils {
    // 私有构造
    private ExecutorServiceUtils() {}

    // 静态内部类
    private static class InnerOpsExecutorService{
        private static ExecutorService executorService = Executors.newFixedThreadPool(4);
    }

    public static ExecutorService getInstance() {
        return InnerOpsExecutorService.executorService;
    }
}
