package com.anjingsi.tools.queue;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Scanner;

/**
 * @program: 数组实现队列
 * @description
 * @author: 安静思
 * @create: 2019-12-11 14:18
 **/
public class ArrayQueue{

    private Object[] array;

    /**
     * 队列最后的位置
     */
    private int rear;

    /**
     * 队列最前面的元素
     */
    private int front;

    private int length;

    public <T>ArrayQueue(int maxSize){
        this.array = new Object[maxSize];
        this.rear = -1 ;
        this.front = -1;
        length = maxSize;
        List<String> list = Lists.newArrayList();
    }

    public void addQueue(Object obj){
        if(isFull()){
            System.out.println("队列已满---");
            return;
        }
        if(isEmpty()){
            front = 0;
        }
        rear++;
        array[rear % length] = obj;
    }

    /**
     * 取出队列头部的元素
     * @return
     */
    public Object getQueue(){
        if(isEmpty()){
            throw new RuntimeException("队列是空的--");
        }
        Object obj = array[front];
        array[front] = null;
        if(front == rear){
            front = -1;
            rear = -1;
        }else {
            front++;
            front = front % length;
        }
        return obj;
    }

    /**
     * 查看头部元素
     */
    public Object showHeadQueue(){
        if(isEmpty()){
            throw new RuntimeException("队列是空的--");
        }
        return array[front];
    }

    /**
     * 查看队列所有元素
     */
    public void showQueue(){
        if(isEmpty()){
            throw new RuntimeException("队列是空的--");
        }
        int size = queueSize();
        for(int i = 0 ; i < size ; i ++){
            System.out.print(array[(front + i)%length]+"\t");
        }
        System.out.println();
    }

    /**
     * 查看当前队列的有效数据个数
     */
    public int queueSize(){
        if(isEmpty()){
            return 0;
        }
        return (length + rear - front + 1) % length == 0 ? length : (length + rear - front + 1) % length;
    }

    /**
     * 判断队列是否为空
     * @return
     */
    public boolean isEmpty(){
        return rear == -1 && front == -1;
    }

    /**
     * 判断队列是否满
     * @return
     */
    public boolean isFull(){
        return (length + rear - front + 1) % length == 0;
    }

    public static void main(String[] args) {
        ArrayQueue arrayQueue = new ArrayQueue(5);
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        while (flag){
            System.out.println("a添加 g取出头部元素 s查看头部元素 e退出 w查看所有元素");
            System.out.println("请输入你想要的操作--");
            String str = scanner.next();
            switch (str){
                case "a":
                    System.out.println("请输入你要添加的数据");
                    String str1 =  scanner.next();
                    arrayQueue.addQueue(str1);
                    break;
                case "g":
                    try{
                        Object queue = arrayQueue.getQueue();
                        System.out.println("取出去的头元素为："+queue);
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case "s":
                    try{
                        Object o = arrayQueue.showHeadQueue();
                        System.out.println("头元素为："+o);
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case "e":
                    flag = false;
                    break;
                case "w":
                    try{
                        arrayQueue.showQueue();
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                default:
                    System.out.println("没有该操作");
            }
        }
        scanner.close();
        System.out.println("程序退出---");
    }
}
