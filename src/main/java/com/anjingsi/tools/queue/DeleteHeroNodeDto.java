package com.anjingsi.tools.queue;

import com.anjingsi.tools.dto.HeroNode;
import lombok.Data;

/**
 * @program: jaas-cloud-cxy
 * @description
 * @author: 安静思
 * @create: 2019-12-06 14:00
 **/
@Data
public class DeleteHeroNodeDto {

    private int index;

    private HeroNode heroNode;
}
