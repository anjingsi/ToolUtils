package com.anjingsi.tools.test;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.RecursiveTask;

@Slf4j
public class ForkJoinWork extends RecursiveTask<Long> {

    private Long start;

    private Long end;

    public static final  Long critical = 1000000000L;//临界值

    public ForkJoinWork(Long start,Long end){
        this.start = start;
        this.end = end;
    }

    @Override
    protected Long compute() {
        log.info("现在的开始值为：" +start);
        Long len = end - start;
        if(len <= critical){
            Long sum = 0L;
            for (int i = 0; i <= end; i++) {
                sum += i;
            }
            return sum;
        }else {
            Long middle = (end - start) / 2 ;
            ForkJoinWork right = new ForkJoinWork(start,middle);
            right.fork();
            ForkJoinWork left = new ForkJoinWork(middle+1,end);
            left.fork();//拆分，并压入线程队列
            return right.join()+left.join();
        }
    }
}