package com.anjingsi.tools.test;

import lombok.Data;

import java.io.Serializable;

/*

 集合，数据
 流，计算！

*/
@Data
public class User implements Serializable {

    private int id;
    private String userName;
    private int age;

    public User() {
    }

    public User(int id, String userName, int age) {
        this.id = id;
        this.userName = userName;
        this.age = age;
    }
}
