package com.anjingsi.tools.test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * @program: jaas-cloud-cxy
 * @description
 * @author: 安静思
 * @create: 2019-12-04 17:19
 **/
public class Test {
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {
//        EnumSingleton enumSingleton1 = EnumSingleton.INSTANCE;
//        EnumSingleton enumSingleton2 = EnumSingleton.INSTANCE;
//        System.out.println(enumSingleton1 == enumSingleton2);
//
//        Constructor<EnumSingleton> declaredConstructor = EnumSingleton.class.getDeclaredConstructor(null);
//        EnumSingleton enumSingleton = declaredConstructor.newInstance();
//        System.out.println(enumSingleton == enumSingleton1);

        //        LazyMan lazyMan1 = LazyMan.getInstance();
//        System.out.println(lazyMan1.hashCode());

//        Constructor<LazyMan> constructor = LazyMan.class.getDeclaredConstructor(null);
//        constructor.setAccessible(true);
//        LazyMan lazyMan2 = constructor.newInstance();
//        System.out.println(lazyMan2.hashCode());
//
//        Field flag = LazyMan.class.getDeclaredField("flag");
//        flag.setAccessible(true);
//        flag.set("flag",false);
//        LazyMan lazyMan3 = constructor.newInstance();
//        System.out.println(lazyMan3.hashCode());

//        User u1 = new User(11, "a", 23);
//        User u2 = new User(12, "b", 24);
//        User u3 = new User(13, "e", 22);
//        User u4 = new User(14, "d", 28);
//        User u5 = new User(16, "c", 26);
//
//        List<User> list = Arrays.asList(u1,u2,u3,u4,u5);
//        list.stream().filter(user -> user.getAge()%2 == 0)
//                .filter(user -> user.getAge() > 24)
//                .map(user -> user.getUserName().toUpperCase())
//                .sorted((o1,o2)->o1.compareTo(o2))
//                .limit(1)
//                .forEach(System.out::println);


    }
}
