package com.anjingsi.tools.date;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

@Slf4j
public class DateUtils {

    public static String getRegexp(String crateTime) throws ParseException {
        if(StringUtils.isEmpty(crateTime)){
            throw new RuntimeException("时间字符串不能为空");
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Long time = sdf.parse(crateTime).getTime();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(time));
        DecimalFormat df=new DecimalFormat("00");
        String begin=df.format(calendar.get(Calendar.MINUTE));
        calendar.add(Calendar.MINUTE,30);
        int end  = calendar.get(Calendar.MINUTE);
        String regexp = "..:"+begin+":..|..:"+end+":..";
        return regexp;
    }

    public static String getDate(Long time){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(time));
    }

    public static boolean isDateTime(String datetime){
        String timeRegex = "^((([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-02-29))\\s+([0-1]?[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$";
        Pattern p = Pattern.compile(timeRegex);
        return p.matcher(datetime).matches();
    }
}
