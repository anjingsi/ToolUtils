package com.anjingsi.tools.database;



import java.io.Serializable;
import java.util.List;

public class PaginationSupport<T> implements Serializable {
    private int pageSize;
    private int pageNum;
    private int total;
    private int pageCount;
    private List<T> items;

    private void init() {
        if (this.total > 0) {
            this.pageCount = this.total / this.pageSize;
            if (this.total % this.pageSize > 0) {
                ++this.pageCount;
            }
        } else {
            this.pageCount = 0;
        }

    }

    public PaginationSupport() {
    }

    public PaginationSupport(List<T> items, int total, int pageSize, int pageNum) {
        this.pageSize = pageSize;
        this.pageNum = pageNum;
        this.total = total;
        this.items = items;
        this.init();
    }

//    public PaginationSupport(List<T> items, PageHelper pageHelper) {
//        this.pageSize = PageHelper.getPagination().getSize();
//        this.pageNum = PageHelper.getPagination().getCurrent();
//        this.total = (int)PageHelper.freeTotal();
//        this.items = items;
//        this.init();
//    }

    public int getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return this.pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getTotal() {
        return this.total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPageCount() {
        return this.pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public List<T> getItems() {
        return this.items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}

