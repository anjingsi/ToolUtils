package com.anjingsi.tools.config;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 线程池配置
 *
 * @author anyp
 */
public class ExecutorServiceConfig {
    private ExecutorServiceConfig() {}

    private static class InnerOpsExecutorService{
        private static ExecutorService executorService = Executors.newFixedThreadPool(4);
    }

    public static ExecutorService getInstance() {
        return InnerOpsExecutorService.executorService;
    }
}
