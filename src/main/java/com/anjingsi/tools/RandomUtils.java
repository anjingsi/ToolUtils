package com.anjingsi.tools;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * @program: zssj-parent-project
 * @description  随机字符
 * @author: 安静思
 * @create: 2019-06-12 13:31
 **/
public class RandomUtils {
    private static SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");

    private static String numCode = "0123456789";

    /**
     * 随机数字验证码
     * @param n
     * @return
     */
    public static String getNumCode(int n) {
        char[] ch = new char[n];
        for (int i = 0; i < n; i++) {
            Random random = new Random();
            int index = random.nextInt(numCode.length());
            ch[i] = numCode.charAt(index);
        }
        String result = String.valueOf(ch);
        return result;
    }

    /**
     * 随机生成字符串
     * 无 -
     * @return
     */
    public static String randomUUIDStr(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }

    /**
     * 随机生成uuid
     * @return
     */
    public static String randomUUID(){
        return UUID.randomUUID().toString();
    }

    /**
     * 随机生成Long
     * @return
     */
    public static Long randomLongUUID(){
        Long id =  UUID.randomUUID().getLeastSignificantBits();
        if(id < 0){
            id = -id;
        }
        return id;
    }

    /**
     * 随机生成时间前缀的字符串
     */
    public static String randomTimePrefix(){
        return df.format(new Date())+ RandomUtils.getNumCode(8);
    }

}
