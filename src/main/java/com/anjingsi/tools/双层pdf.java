//package com.anjingsi.tools;
//
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
//import com.alibaba.fastjson.parser.Feature;
//import com.lowagie.text.Document;
//import com.lowagie.text.Image;
//import com.lowagie.text.Rectangle;
//import com.lowagie.text.pdf.BaseFont;
//import com.lowagie.text.pdf.PdfContentByte;
//import com.lowagie.text.pdf.PdfGState;
//import com.lowagie.text.pdf.PdfWriter;
//
//import javax.imageio.ImageIO;
//import java.awt.*;
//import java.awt.geom.AffineTransform;
//import java.awt.image.AffineTransformOp;
//import java.awt.image.BufferedImage;
//import java.io.*;
//import java.nio.MappedByteBuffer;
//import java.nio.channels.FileChannel;
//import java.util.Iterator;
//
//public class PdfDemo {
//    public static final char ASCII_END = 126;
//    public static final char ASCII_NUM_STRAT = 48;
//    public static final char ASCII_NUM_END = 57;
//    public static final char UNICODE_NUM_START = 65296;
//    public static final char UNICODE_NUM_END = 65305;
//    // 全角半⻆符号转化之间的间�?
//    public static final char DBC_SBC_STEP = 65248;
//    private final static String RESULT_DATA = "data";
//    private final static String RESULT_ANGLE = "angle";
//    private final static String RESULT_WORDS_INFO = "prism_wordsInfo";
//
//
//    public static void main(String[] args) throws Exception {
//        // ocr识别结果，复制进�?
//        String ocrResult = "";
//        // 或�?��?�过读文件方式获取ocr识别结果
//        // byte[] ocrResultByte = getBytesOfFile("/Users/jack/tmp/result.txt");
//        // String ocrResult = new String(ocrResultByte);
//
//        // 生成的pdf文件路径
//        String resultPath = "/Users/jack/tmp/test.pdf";
//        // 原图片内容，作为pdf背景
//        byte[] imgData = getBytesOfFile("/Users/jack/Desktop/9.jpg");
//        // 文字透明�?
//        float opacity = 0.8f;
//        JSONObject ocrResultJson = JSONObject.parseObject(ocrResult, Feature.OrderedField);
//        int angle = ocrResultJson.getInteger(RESULT_ANGLE);
//        JSONArray prismWordsInfo = ocrResultJson.getJSONArray(RESULT_WORDS_INFO);
//        generatePdfFile(imgData, prismWordsInfo, resultPath, angle, opacity);
//    }
//
//    /**
//     * 读取文件内容
//     *
//     * @param filePath 文件路径
//     * @throws IOException
//     */
//    private static byte[] getBytesOfFile(String filePath) throws IOException {
//        File file = new File(filePath);
//        FileChannel fileChannel = null;
//        byte[] contentByte = null;
//        try {
//            fileChannel = new RandomAccessFile(file, "r").getChannel();
//            MappedByteBuffer buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size());
//            contentByte = new byte[buffer.limit()];
//            for (int i = 0; i < buffer.limit(); i++) {
//                contentByte[i] = buffer.get();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (fileChannel != null) {
//                fileChannel.close();
//            }
//        }
//        return contentByte;
//    }
//
//    /**
//     * 生成pdf文件
//     *
//     * @param imgData    图⽚内容
//     * @param ocrResult  图片识别结果中的prism_wordsInfo信息
//     * @param resultPath ⽣成的pdf文件路路�?
//     * @param angle      图片旋转⻆角�?
//     * @throws IOException
//     */
//    private static void generatePdfFile(byte[] imgData, JSONArray ocrResult, String
//            resultPath, Integer angle, Float opacity) throws IOException {
//        Document pdfDocument = new Document();
//        PdfWriter pdfWriter = PdfWriter.getInstance(pdfDocument, new FileOutputStream(resultPath));
//        generatePdf(imgData, ocrResult, pdfDocument, pdfWriter, opacity, angle);
//    }
//
//    /**
//     * 生成pdf
//     *
//     * @param imgData     图片内容
//     * @param ocrResult   图片识别结果中的prism_wordsInfo信息
//     * @param pdfDocument pdfDocument
//     * @param opacity     背景图片透明�?
//     * @param angle
//     * @throws IOException
//     */
//    private static void generatePdf(byte[] imgData, JSONArray ocrResult, Document pdfDocument, PdfWriter pdfWriter, float opacity, Integer angle) throws IOException {
//        // 旋转后的宽度
//        int width = 0;
//        // 旋转后的高度
//        int height = 0;
//        BufferedImage bufferedImage = null;
//        ByteArrayInputStream byteArrayInputStream = null;
//        try {
//            byteArrayInputStream = new ByteArrayInputStream(imgData);
//            bufferedImage = ImageIO.read(byteArrayInputStream);
//            width = bufferedImage.getWidth();
//            height = bufferedImage.getHeight();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (byteArrayInputStream != null) {
//                    byteArrayInputStream.close();
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        if (angle != 0) {
//            // 逆时针旋转化成顺时针
//            angle = 360 - angle;
//            // 将角度转为弧�?
//            double theta = Math.toRadians(angle);
//            // 确定旋转后的宽和�?
//            int newWidth = (int) (Math.abs(Math.sin(theta)) * height +
//                    Math.abs(Math.cos(theta)) * width);
//            int newHeight = (int) (Math.abs(Math.sin(theta)) * width +
//                    Math.abs(Math.cos(theta)) * height);
//            BufferedImage spinImage = new BufferedImage(newWidth, newHeight,
//                    bufferedImage.getType());
//            // 设置图⽚背景颜色
//            Graphics2D gs = (Graphics2D) spinImage.getGraphics();
//            gs.setColor(Color.white);
//            // 以给定颜色绘制旋转后图片的背�?,fillRect:对指定的矩形区域填充颜色
//            gs.fillRect(0, 0, newWidth, newHeight);
//            // 确定原点坐标
//            int x = (newWidth / 2) - (width / 2);
//            int y = (newHeight / 2) - (height / 2);
//            AffineTransform at = new AffineTransform();
//            // 旋转图象
//            at.rotate(theta, newWidth / 2, newHeight / 2);
//            at.translate(x, y);
//            AffineTransformOp op = new AffineTransformOp(at, AffineTransformOp.TYPE_BICUBIC);
//            spinImage = op.filter(bufferedImage, spinImage);
//            ByteArrayOutputStream out = null;
//            try {
//                out = new ByteArrayOutputStream();
//                ImageIO.write(spinImage, "jpg", out);
//                imgData = out.toByteArray();
//            } catch (IOException e) {
//                e.printStackTrace();
//            } finally {
//                try {
//                    if (out != null) {
//                        out.close();
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            width = newWidth;
//            height = newHeight;
//        }
//        // 设置pdf⼤小
//        Rectangle rectangle = new Rectangle(width, height, 0);
//        pdfDocument.setPageSize(rectangle);
//        pdfDocument.open();
//        // 获取原图和pdf的缩放⽐�?
//        float xScale = width / rectangle.getWidth();
//        float yScale = height / rectangle.getHeight();
//        PdfContentByte canvas = pdfWriter.getDirectContentUnder();
//        Image image = Image.getInstance(imgData);
//        image.scaleAbsolute(image.getWidth(), image.getHeight());
//        image.setAbsolutePosition(0, 0);
//        // 加载背景图片
//        canvas.addImage(image);
//        canvas = pdfWriter.getDirectContent();
//        // y轴坐标反�?
//        canvas.concatCTM(1f, 0f, 0f, 1f, 0f, height);
//        // 解析图⽚片识别结果并⽣生成pdf
//        BaseFont baseFont = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H", false);
//        // 系统字体对中文支持不好，推荐更换为其他字体，⽐如simhei.ttf�? 更换方式: 将下载的字体放到本地目录，例�?/Users/jack/tmp/simhei.ttf 代码:
//        //BaseFont baseFont = BaseFont.createFont("/Users/jack/tmp/simhei.ttf", "Identity-H", false);
//        // 设置文字颜色
//        canvas.setColorFill(new Color(0xFF, 0x00, 0x00));
//        // 设置文字透明�?
//        canvas.saveState();
//        PdfGState state = new PdfGState();
//        state.setFillOpacity(opacity);
//        canvas.setGState(state);
//        for (Iterator it = ocrResult.iterator(); it.hasNext(); ) {
//            JSONObject jsonObject = (JSONObject) it.next();
//            JSONArray charInfos = jsonObject.getJSONArray("charInfo");
//            for (Iterator iterator = charInfos.iterator(); iterator.hasNext(); ) {
//                JSONObject charInfo = (JSONObject) iterator.next();
//                String word = charInfo.getString("word");
//                // 全⻆半⻆转化
//                word = changePdfResult(word);
//                int x = charInfo.getInteger("x") + charInfo.getInteger("w") / 2;
//                int y = charInfo.getInteger("y") + charInfo.getInteger("h");
//                // pdf增加文字
//                float fontSize = charInfo.getInteger("h") / yScale;
//                canvas.setFontAndSize(baseFont, fontSize);
//                canvas.beginText();
//                canvas.showTextAligned(PdfContentByte.ALIGN_CENTER, word, x / xScale, -(y / yScale), 0);
//                canvas.endText();
//            }
//        }
//        canvas.restoreState();
//        pdfDocument.close();
//    }
//
//    // 将字符串中的全角数字转化为半角数字，标点转化为全�?
//    private static String changePdfResult(String src) {
//        if (src == null) {
//            return null;
//        }
//        char[] c = src.toCharArray();
//        for (int i = 0; i < c.length; i++) {
//            if (c[i] < ASCII_NUM_STRAT || (c[i] > ASCII_NUM_END && c[i] <= ASCII_END)) {
//                // 除了数字以外的符号，半角转全�?
//                c[i] += DBC_SBC_STEP;
//            } else if (c[i] >= UNICODE_NUM_START && c[i] <= UNICODE_NUM_END) {
//                // 数字全角转半�?
//                c[i] -= DBC_SBC_STEP;
//            }
//        }
//        return new String(c);
//    }
//}
