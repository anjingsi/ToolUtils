//package com.anjingsi.tools;
//
//import com.google.common.collect.Lists;
//import com.zssj.cxy.dto.Tess4JDto;
//import lombok.extern.slf4j.Slf4j;
//import net.sourceforge.tess4j.ITesseract.RenderedFormat;
//import net.sourceforge.tess4j.Tesseract;
//import net.sourceforge.tess4j.TesseractException;
//
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//import java.util.concurrent.CountDownLatch;
//import java.util.concurrent.ExecutorService;
//
//@Slf4j
//public class Tess4JUtils {
//
//    static final double MINIMUM_DESKEW_THRESHOLD = 0.05d;
//
//    static List<RenderedFormat> formats = new ArrayList<RenderedFormat>(Arrays.asList(RenderedFormat.PDF));
//
//    private Tesseract initTesseract(String tessdata){
//        Tesseract instance = new Tesseract();
//        //设置训练位置
//        instance.setDatapath(tessdata);
//        //设置语言�?
//        instance.setLanguage("chi_sim");
//        return instance;
//    }
//
//    /**
//     * 转换单张图片为双层PDF
//     * @throws Exception
//     */
//    public void imgCreateDocuments(Tess4JDto tess4JDto,String tessdata) throws TesseractException {
//        long time = System.currentTimeMillis();
//        log.info("�?始单张转换图片到PDF");
//        log.info("当前系统的训练的路径是："+ tessdata);
//        Tesseract tesseract = initTesseract(tessdata);
//        tesseract.createDocuments(tess4JDto.getInputFilePath(),tess4JDto.getOutFilePath(),formats);
//        log.info("转换成功，�?�时�?"+(System.currentTimeMillis()-time));
//    }
//
//    /**
//     * 将多张图片转换为双层PDF
//     * @throws Exception
//     */
//    public void imgsCreateDocuments(Tess4JDto tess4JDto,String tessdata) throws TesseractException, IOException {
//        long time = System.currentTimeMillis();
//        List<String> inputImgFile = ImgUtils.getImgsByFolder(tess4JDto.getInputFilePath());
//        String[] inputs = inputImgFile.toArray(new String[inputImgFile.size()]);
//        log.info("文件的个数为�?"+inputs.length);
//        if(inputs.length == 0){
//            return;
//        }
//        String[] outputs = createOutputs(inputs);
//        log.info("�?始多张转换图片到PDF");
//        Tesseract tesseract = initTesseract(tessdata);
//        tesseract.createDocuments(inputs,outputs,formats);
//        log.info("转换成功，�?�时�?"+(System.currentTimeMillis()-time));
//    }
//
//    /**
//     * 通过多线程转换多张图�?
//     * @throws Exception
//     */
//    public void imgsCreateDocuments(Tess4JDto tess4JDto,String tessdata,ExecutorService executorService) throws TesseractException, IOException, InterruptedException {
//        long time = System.currentTimeMillis();
//        List<String> inputImgFile = ImgUtils.getImgsByFolder(tess4JDto.getInputFilePath());
//        String[] inputs = inputImgFile.toArray(new String[inputImgFile.size()]);
//        log.info("文件的个数为�?"+inputs.length);
//        if(inputs.length == 0){
//            return;
//        }
//        log.info("�?始采用多线程转换图片到PDF");
//        CountDownLatch countDownLatch = new CountDownLatch(inputs.length);
//        log.info("当前系统的训练的路径是："+ tessdata);
//        for(String input : inputs){
//            executorService.execute(()->{
//                try {
//                    Tesseract tesseract = initTesseract(tessdata);
//                    tesseract.createDocuments(input,input.substring(0,input.lastIndexOf(".")),formats);
//                } catch (TesseractException e) {
//                    e.printStackTrace();
//                }finally {
//                    countDownLatch.countDown();
//                }
//            });
//        }
//        countDownLatch.await();
//        log.info("转换成功，�?�时�?"+(System.currentTimeMillis()-time));
//    }
//
//    private String[] createOutputs(String[] inputs) {
//        List<String> list = Lists.newArrayList();
//        if(inputs.length == 0){
//            return null;
//        }
//        for(String input : inputs){
//            list.add(input.substring(0,input.lastIndexOf(".")));
//        }
//        return list.toArray(new String[list.size()]);
//    }
//
//    /**
//     * 将图片文件进行识�?
//     * @throws Exception while processing image.
//     */
//    public String imgFileDoOCR(Tess4JDto tess4JDto,String tessdata) throws TesseractException {
//        long time = System.currentTimeMillis();
//        File imageFile = new File(tess4JDto.getInputFilePath());
//        Tesseract tesseract = initTesseract(tessdata);
//        String result = tesseract.doOCR(imageFile);
//        log.info("识别的结果为�?"+result);
//        log.info("识别成功，�?�时�?"+(System.currentTimeMillis()-time));
//        return result;
//    }
//
//    /**
//     * 根据图片流进行识�?
//     * @throws Exception while processing image.
//     */
//    public String imageBufferedDoOCR(BufferedImage bi,String tessdata) throws Exception {
//        long time = System.currentTimeMillis();
//        Tesseract tesseract = initTesseract(tessdata);
//        String result = tesseract.doOCR(bi);
//        log.info("识别的结果为�?"+result);
//        log.info("识别成功，�?�时�?"+(System.currentTimeMillis()-time));
//        return result;
//    }
//
////    public static void main(String ags[]) throws Exception{
////    	Tess4JUtils tess4JTest = new Tess4JUtils();
////    	tess4JTest.instance = new Tesseract();
////    	tess4JTest.instance.setDatapath(new File(tess4JTest.datapath).getPath());
////    	tess4JTest.testCreateDocuments();
////    }
//
//    /**
//     * Test of getSegmentedRegions method, of class Tesseract.
//     * 得到每一个划分区域的具体坐标
//     * @throws Exception
//     */
//    public void testGetSegmentedRegions() throws Exception {
////        log.info("getSegmentedRegions at given TessPageIteratorLevel");
////        File imageFile = new File(testResourcesDataPath, "123.jpg");
////        BufferedImage bi = ImageIO.read(imageFile);
////        int level = TessPageIteratorLevel.RIL_SYMBOL;
////        log.info("PageIteratorLevel: " + Utils.getConstantName(level, TessPageIteratorLevel.class));
////        List<Rectangle> result = instance.getSegmentedRegions(bi, level);
////        for (int i = 0; i < result.size(); i++) {
////            Rectangle rect = result.get(i);
////            log.info(String.format("Box[%d]: x=%d, y=%d, w=%d, h=%d", i, rect.x, rect.y, rect.width, rect.height));
////        }
//
//    }
//
//
//    /**
//     * Test of doOCR method, of class Tesseract.
//     * 根据定义坐标范围进行识别
//     * @throws Exception while processing image.
//     */
//    public void testDoOCR_File_Rectangle() throws Exception {
////        log.info("doOCR on a BMP image with bounding rectangle");
////        File imageFile = new File(this.testResourcesDataPath, "123.jpg");
////        //设置语言�?
////        instance.setDatapath(testResourcesLanguagePath);
////        instance.setLanguage("chi_sim");
////        //划定区域
////        // x,y是以左上角为原点，width和height是以xy为基�?
////        Rectangle rect = new Rectangle(84, 21, 15, 13);
////        String result = instance.doOCR(imageFile, rect);
////        log.info(result);
//    }
//
//    /**
//     * Test of getWords method, of class Tesseract.
//     * 取词方法
//     * @throws Exception
//     */
//    public void testGetWords() throws Exception {
////        log.info("getWords");
////        File imageFile = new File(this.testResourcesDataPath, "123.jpg");
////
////        //设置语言�?
////        instance.setDatapath(testResourcesLanguagePath);
////        instance.setLanguage("chi_sim");
////
////        //按照每个字取�?
////        int pageIteratorLevel = TessPageIteratorLevel.RIL_SYMBOL;
////        log.info("PageIteratorLevel: " + Utils.getConstantName(pageIteratorLevel, TessPageIteratorLevel.class));
////        BufferedImage bi = ImageIO.read(imageFile);
////        List<Word> result = instance.getWords(bi, pageIteratorLevel);
////
////        //print the complete result
////        for (Word word : result) {
////            log.info(word.toString());
////        }
//    }
//
//    /**
//     * Test of Invalid memory access.
//     * 处理倾斜
//     * @throws Exception while processing image.
//     */
//    public void testDoOCR_SkewedImage() throws Exception {
////        //设置语言�?
////        instance.setDatapath(testResourcesLanguagePath);
////        instance.setLanguage("chi_sim");
////
////        log.info("doOCR on a skewed PNG image");
////        File imageFile = new File(this.testResourcesDataPath, "ocr_skewed.jpg");
////        BufferedImage bi = ImageIO.read(imageFile);
////        ImageDeskew id = new ImageDeskew(bi);
////        double imageSkewAngle = id.getSkewAngle(); // determine skew angle
////        if ((imageSkewAngle > MINIMUM_DESKEW_THRESHOLD || imageSkewAngle < -(MINIMUM_DESKEW_THRESHOLD))) {
////            bi = ImageHelper.rotateImage(bi, -imageSkewAngle); // deskew image
////        }
////
////        String result = instance.doOCR(bi);
////        log.info(result);
//    }
//
//}
