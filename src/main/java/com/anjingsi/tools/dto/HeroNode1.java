package com.anjingsi.tools.dto;

import lombok.Data;


/**
 * 节点
 */
@Data
public class HeroNode1 {
    private int no;
    private String name;
    private String nickName;
    /**
     * 指向上一个节点
     */
    private HeroNode1 pro;
    //构造器
    public HeroNode1(int no, String name, String nickName) {
        this.no = no;
        this.name = name;
        this.nickName = nickName;
    }

    @Override
    public String toString() {
        return "HeroNode{" +
                "no=" + no +
                ", name='" + name + '\'' +
                ", nickName='" + nickName + '\''+
                '}';
    }
}