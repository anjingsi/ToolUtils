package com.anjingsi.tools.list;

import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.BeanUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author anyp
 * @since 2018-12-11
 */
public class ListUtils {
    public ListUtils() {

    }

    public static List<Long> listLong(String strings) {
        return listLong(strings, "," );
    }

    public static List<Long> listLong(String strings, String splitor) {
        if (strings != null && !strings.trim().isEmpty()) {
            if (splitor == null) {
                splitor = "," ;
            }

            List<Long> ret = new ArrayList();
            Arrays.asList(strings.split(splitor)).stream().mapToLong((t) -> {
                return Long.valueOf(t).longValue();
            }).forEach((string) -> {
                ret.add(string);
            });
            return ret;
        } else {
            return Collections.emptyList();
        }
    }

    public static List<String> listString(String strings) {
        return listString(strings, "," );
    }

    public static List<String> listString(String strings, String splitor) {
        if (strings != null && !strings.trim().isEmpty()) {
            if (splitor == null) {
                splitor = "," ;
            }

            List<String> ret = new ArrayList();
            Arrays.asList(strings.split(splitor)).forEach((string) -> {
                ret.add(string);
            });
            return ret;
        } else {
            return Collections.emptyList();
        }
    }

    public static <E> E fetchOne(Collection<E> targets) {
        if (targets != null && !targets.isEmpty()) {
            Iterator var1 = targets.iterator();
            if (var1.hasNext()) {
                E e = (E) var1.next();
                return e;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static <T> T copy(Object source, Class<T> target) {
        try {
            T o = target.newInstance();
            BeanUtils.copyProperties(source, o);
            return o;
        } catch (Exception var3) {
            var3.printStackTrace();
            return null;
        }
    }

    public static <T> List<T> copyList(Collection source, Class<T> target) {
        return source == null ? Collections.emptyList() : (List) source.stream().map((s) -> {
            return copy(s, target);
        }).collect(Collectors.toList());
    }

    /**
     * 将List<String>集合 转化为String
     * 如{"aaa","bbb"} To aaa,bbb
     */
    public static <E> String convertListToString(Collection<E> targets, String splitor) {
        StringBuffer sb = new StringBuffer();
        if (CollectionUtils.isNotEmpty(targets)) {
            for (E e : targets) {
                if (sb.length() > 0) {
                    sb.append("," ).append(e);
                } else {
                    sb.append(e);
                }
            }
        }
        return sb.toString();
    }

    /**
     * 使用HashSet实现List去重(无序)
     *
     * @param list
     */
    public static List<T> removeDuplicationByHashSet(List<T> list) {
        HashSet set = new HashSet(list);
        //把List集合所有元素清空
        list.clear();
        //把HashSet对象添加至List集合
        list.addAll(set);
        return list;
    }

    /**
     * 使用TreeSet实现List去重(有序)
     *
     * @param list
     */
    public static List<T> removeDuplicationByTreeSet(List<T> list) {
        TreeSet set = new TreeSet(list);
        //把List集合所有元素清空
        list.clear();
        //把HashSet对象添加至List集合
        list.addAll(set);
        return list;
    }

    /**
     * 使用java8新特性stream实现List去重(有序)
     *
     * @param list
     */
    public static List<T> removeDuplicationByStream(List<T> list) {
        return list.stream().distinct().collect(Collectors.toList());
    }


}
