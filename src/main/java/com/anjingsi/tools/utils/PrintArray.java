package com.anjingsi.tools.utils;

/**
 * @program: 打印工具类
 * @description
 * @author: 安静思
 * @create: 2019-12-19 14:02
 **/
public class PrintArray {

    /**
     * 打印二维数组
     * @param array
     */
    public static void printArray(int[][] array){
        System.out.println("---------------------------------------------------");
        for (int i = 0 ; i < array.length ; i++){
            for (int j = 0 ; j < array[i].length ; j++){
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
