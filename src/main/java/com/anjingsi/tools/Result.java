
package com.anjingsi.tools;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 响应数据
 *
 * @author anyp
 * @since 1.0.0
 */
@Data
@ApiModel(value = "响应")
public class Result<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 编码：0表示成功，其他值表示失败
     */
    @ApiModelProperty(value = "编码：0表示成功，其他值表示失败")
    private int code;
    /**
     * 消息内容
     */
    @ApiModelProperty(value = "消息内容")
    private String message;
    /**
     * 响应数据
     */
    @ApiModelProperty(value = "响应数据")
    private T data;

    /**
     * 异常的信息
     */
    @ApiModelProperty(value = "异常的信息")
    private String errorMsg;

    public Result(){
    }

    public static <E> Result<E> ok() {
        return new Result<>().success();
    }

    public static <E> Result ok(E data) {
        return new Result<>().success(data);
    }

    public Result success() {
        this.code = 0;
        this.setMessage("成功");
        return this;
    }

    public Result success(T data) {
        this.success();
        this.setData(data);
        return this;
    }

    public static <E> Result<E> fail() {
        return new Result<>().error();
    }

    public static <E> Result fail(String message) {
        return new Result<>().error(message);
    }

    public static <T> Result fail(String message,T data) {
        Result result = new Result();
        result.setMessage(message);
        result.setData(data);
        result.code = 1000;
        return result;
    }

    public Result error() {
        this.code = 1000;
        this.message = "error";
        return this;
    }

    public Result<T> error(int code) {
        this.code = code;
        this.message = "error";
        return this;
    }

//    public Result error(T data) {
//        this.error();
//        this.setMessage(data);
//        return this;
//    }

    public Result<T> error(int code, String msg) {
        this.code = code;
        this.message = msg;
        return this;
    }

    public Result<T> error(String msg) {
        this.message = msg;
        this.code = 1000;
        return this;
    }

    public static enum ResponseState {
        FAIL(10000, "失败"),
        SUCCESS(0, "成功");

        private int code;
        private String codeInfo;

        private ResponseState(int code, String codeInfo) {
            this.code = code;
            this.codeInfo = codeInfo;
        }

        public int getCode() {
            return this.code;
        }

        public String getCodeInfo() {
            return this.codeInfo;
        }

        public String toString() {
            return super.toString().toLowerCase();
        }

        public static Result.ResponseState byCode(int code) {
            Result.ResponseState[] states = values();

            for(int i = 0; i < states.length; ++i) {
                Result.ResponseState state = states[i];
                if (state.getCode() == code) {
                    return state;
                }
            }

            return FAIL;
        }
    }

    public class Meta {
        private Result.ResponseState state;
        private String message;

        public Meta() {
        }

        public Meta(Result.ResponseState responseState) {
            this.state = responseState;
            this.message = responseState.getCodeInfo();
        }

        public Meta(Result.ResponseState responseState, String message) {
            this.state = responseState;
            if (message == null) {
                message = responseState.getCodeInfo();
            }

            this.message = message;
        }

        public int getCode() {
            return this.state.getCode();
        }

        public void setCode(Result.ResponseState responseState) {
            this.state = responseState;
        }

        public String getMessage() {
            return this.message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}

