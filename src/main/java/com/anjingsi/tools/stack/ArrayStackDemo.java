package com.anjingsi.tools.stack;

import java.util.Scanner;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-17 11:37
 **/
public class ArrayStackDemo {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayStack arrayStack = new ArrayStack(4);
        boolean flag = true;
        while (flag){
            System.out.println("exit 退出  pop 出栈  push 入栈 show 查看栈中元素");
            System.out.println("请输入你想要的操作");
            String input = scanner.next();
            switch (input){
                case "exit":
                    scanner.close();
                    flag = false;
                    break;
                case "push":
                    System.out.println("输入你想要放入的值：");
                    String obj = scanner.next();
                    arrayStack.push(obj);
                    break;
                case "pop":
                    try{
                        System.out.println(arrayStack.pop());
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case "show":
                    try{
                        arrayStack.printStack();
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                default:
                    System.out.println("没有当前的操作");
            }
        }
    }
}
