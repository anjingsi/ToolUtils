package com.anjingsi.tools.stack;

import lombok.extern.slf4j.Slf4j;

/**
 * @program: 数组实现栈
 * @description
 * @author: 安静思
 * @create: 2019-12-17 11:42
 **/
@Slf4j
public class ArrayStack {

    /**
     * 栈中的数据长度
     */
    private int maxSize;

    /**
     * 数据保存在数组中
     */
    private Object[] arr;

    /**
     * 栈顶
     */
    private int pointer;

    public ArrayStack(int maxSize){
        this.maxSize = maxSize;
        this.arr = new Object[maxSize];
        pointer = -1 ;
    }

    /**
     * 是否满
     * @return
     */
    public boolean isFull(){
        return pointer == maxSize - 1 ;
    }

    /**
     * 是否空
     * @return
     */
    public boolean isEmpty(){
        return pointer == -1;
    }

    /**
     * 入栈
     */
    public boolean push(Object obj){
        if(isFull()){
            log.info("栈满~~~~~~~");
            return false;
        }
        pointer ++ ;
        arr[pointer] = obj;
        return true;
    }

    /**
     * 出栈
     */
    public Object pop(){
        if(isEmpty()){
            throw new RuntimeException("栈是空的····");
        }
        Object obj = arr[pointer];
        pointer -- ;
        return obj;
    }

    /**
     * 打印栈中元素
     */
    public void printStack(){
        if(isEmpty()){
            throw new RuntimeException("栈是空的····");
        }
        for(int i = pointer ; i >= 0 ; i-- ){
            log.info("当前元素的位置：{} ，元素的值为：{} ",i,arr[i]);
        }
    }


}
