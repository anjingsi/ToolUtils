package com.anjingsi.tools.stack;

import com.anjingsi.tools.dto.HeroNode;
import com.anjingsi.tools.dto.HeroNode1;

/**
 * @program: 链表实现栈
 * @description
 * @author: 安静思
 * @create: 2019-12-17 15:01
 **/
public class LinkStack {

    /**
     * 尾节点
     */
    private HeroNode1 end;

    public LinkStack(){

    }

    public boolean isEmpty(){
        return end == null;
    }

    /**
     * 入栈
     */
    public boolean push(HeroNode1 node){
        if(isEmpty()){
            end = node;
            return true;
        }
        node.setPro(end);
        end = node;
        return true;
    }

    /**
     * 出栈
     */
    public HeroNode1 pop(){
        if(isEmpty()){
            throw new RuntimeException("栈中没有元素---");
        }
        HeroNode1 heroNode1 = end;
        end = heroNode1.getPro();
        return heroNode1;
    }

    /**
     * 查看栈中元素
     */
    public void printStack(){
        if(isEmpty()){
            System.out.println("栈中没有元素---");
            return;
        }
        HeroNode1 temp = end;
        while (true) {
            if(temp == null){
                break;
            }
            System.out.println(temp);
            temp = temp.getPro();

        }

    }
}
