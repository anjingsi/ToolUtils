package com.anjingsi.tools.stack;

import com.anjingsi.tools.dto.HeroNode;
import com.anjingsi.tools.dto.HeroNode1;

import java.util.Scanner;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-17 11:37
 **/
public class LinkStackDemo {

    public static void main(String[] args) {
        HeroNode1 heroNode1 = new HeroNode1(1,"宋江","及时雨");
        HeroNode1 heroNode2 = new HeroNode1(2,"林冲","豹子头");
        HeroNode1 heroNode3 = new HeroNode1(3,"卢俊义","卢俊义");
        HeroNode1 heroNode4 = new HeroNode1(4,"吴用","军师");
        LinkStack linkStack = new LinkStack();
        System.out.println("空栈取数据");
        linkStack.printStack();
        linkStack.push(heroNode1);
        linkStack.push(heroNode2);
        System.out.println("放入了两条数据");
        linkStack.printStack();
        System.out.println("取出了一条数据");
        linkStack.pop();
        linkStack.printStack();
        System.out.println("放入了两条数据");
        linkStack.push(heroNode3);
        linkStack.push(heroNode4);
        linkStack.printStack();
    }
}
