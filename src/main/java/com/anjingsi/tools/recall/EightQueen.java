package com.anjingsi.tools.recall;

public class EightQueen {
    static int qipan[][] = new int[8][8];
    static int count = 0;
    static int step = 1;
    static int vis[][] = new int[3][20]; //三种情况主对角线，副对角线，行有没有被占用。
    public static void main(String[] args) {
        //初始化棋盘
        for(int i=0;i<8;i++){
            for(int j=0;j<8;j++){
                qipan[i][j] = 0;
            }
        }
        for(int i=0;i<vis.length;i++){
            for(int j=0;j<vis[i].length;j++){
                vis[i][j] = 0;
            }
        }
         
        move(0);
        //从第0个位置开始放第一个皇后，没得说，只要求出皇后的位置，和摆放的数量即可。
        System.out.println("count = " + count);
    }
    public static void move(int x) {
        int next_x = x+1;
        if(step>8){
            for(int i=0;i<8;i++){
                for(int j=0;j<8;j++){
                    if(qipan[i][j] == 0){
                        System.out.printf("%3c",'*'); //把没有皇后，棋盘是0的位置用*输出，显得好看一点
                    }else{
                        System.out.printf("%3d",qipan[i][j]);
                    }
                     
                }
                System.out.println();
            }
            System.out.println("========================");
            count++;
        }else{
            for(int i=0;i<8;i++){
                if(vis[0][i]==0 && vis[1][x+i]==0 && vis[2][x-i+8]==0){//满足摆放条件
                    qipan[x][i] = step;
                    step++;
                    vis[0][i]=1; vis[1][x+i]=1; vis[2][x-i+8]=1;
                    move(next_x);
                    qipan[x][i] = 0;
                    vis[0][i]=0; vis[1][x+i]=0; vis[2][x-i+8]=0;
                    step--;
                     
                }
            }
        }
    }
}