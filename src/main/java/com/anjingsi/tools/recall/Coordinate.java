package com.anjingsi.tools.recall;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-19 14:26
 **/
@Data
public class Coordinate implements Serializable {

    private int row ;

    private int column;

    public Coordinate(){}

    public Coordinate(int row,int column){
        this.row = row ;
        this.column = column;
    }

}
