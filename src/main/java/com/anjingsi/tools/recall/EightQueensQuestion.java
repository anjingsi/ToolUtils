package com.anjingsi.tools.recall;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * @program: ToolUtils
 * @description 八皇后问题
 * @author: 安静思
 * @create: 2019-12-20 09:45
 **/
public class EightQueensQuestion {

    List<int[]> total = Lists.newArrayList();

    /**
     * 判断两坐标是否在同一行上
     * @param firstCoordinate 第一个坐标
     * @param secondCoordinate 第二个坐标
     * @return
     */
    private boolean isSameRow(Coordinate firstCoordinate,Coordinate secondCoordinate){
        return firstCoordinate.getRow() == secondCoordinate.getRow();
    }

    /**
     * 判断两坐标是否在同一列上
     * @param firstCoordinate 第一个坐标
     * @param secondCoordinate 第二个坐标
     * @return
     */
    private boolean isSameColumn(Coordinate firstCoordinate,Coordinate secondCoordinate){
        return firstCoordinate.getColumn() == secondCoordinate.getColumn();
    }

    /**
     * 判断两坐标是否在同一斜线上
     * @param firstCoordinate 第一个坐标
     * @param secondCoordinate 第二个坐标
     * @return
     */
    private boolean isSameObliqueLine(Coordinate firstCoordinate,Coordinate secondCoordinate){
        return Math.abs(firstCoordinate.getRow()-secondCoordinate.getRow()) == Math.abs(firstCoordinate.getColumn()-secondCoordinate.getColumn());
    }

    /**
     * 判断两个点是否符合条件
     * @param firstCoordinate 第一个坐标
     * @param secondCoordinate 第二个坐标
     * @return
     */
    private boolean isMeetCondition(Coordinate firstCoordinate,Coordinate secondCoordinate){
        return !(isSameRow(firstCoordinate,secondCoordinate) || isSameColumn(firstCoordinate,secondCoordinate) || isSameObliqueLine(firstCoordinate,secondCoordinate));
    }

    /**
     * 判断是否符合条件
     * @param coordinate 第一个坐标
     * @param temp 数组坐标
     * @return
     */
    private boolean isMeetCondition(Coordinate coordinate,int[] temp){
        if(temp.length == 0 ){
            return true;
        }
        for (int i = 0; i < temp.length; i++) {
            if(temp[i] == -1){
                break;
            }
            if(isMeetCondition(coordinate,new Coordinate(i,temp[i]))){
                return true;
            }
        }
        return false;
    }

    int[] temp = new int[8];

    private void initTemp(){
        for (int i = 0; i < 8; i++) {
            temp[i] = -1;
        }
    }
    /**
     * 放入的方法
     */
    private void putFunction(){
        for (int i = 0 ; i < 8 ; i++) {
            for (int j = 0; j < 8; j++) {
                temp[i] = j;
            }
        }
    }

    public static void main(String[] args) {
        EightQueensQuestion eightQueensQuestion = new EightQueensQuestion();
        eightQueensQuestion.initTemp();
    }
}
