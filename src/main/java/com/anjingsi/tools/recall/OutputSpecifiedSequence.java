package com.anjingsi.tools.recall;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.*;


/**
 * @program: ToolUtils
 * @description
 * “123” 的输出：  1， 2， 3， 12， 13， 21， 23， 31， 32， 123， 132， 213， 231， 312， 321
 * @author: 安静思
 * @create: 2019-12-20 10:58
 **/
@Slf4j
public class OutputSpecifiedSequence {

    public static void main(String[] args) throws FileNotFoundException {
        String str = "123";
        OutputSpecifiedSequence outputSpecifiedSequence = new OutputSpecifiedSequence();
        outputSpecifiedSequence.test(str);
    }

    public void test(String str){
        int len = str.length();
        String[] array = strToArry(str);
        for (int i = 0; i < str.length(); i++) {
            for (int j = 0; j < array.length; j++) {
                String result = array[j];
                if(result.length() == i+1){
                    System.out.print(result +"\t");
                    continue;
                }else {
                    for (int k = 0; k < array.length; k++) {
                        if(j != k){
                            System.out.print(array[j]+array[k] +"\t");
                        }
                    }
                }
            }
            System.out.println();
        }
    }

    public String[] strToArry(String str){
        if(StringUtils.isBlank(str)){
            throw new RuntimeException("表达式为空");
        }
        String[] arry = new String[str.length()];
        for (int i = 0; i < str.length(); i++) {
            arry[i] = String.valueOf(str.charAt(i));
        }
        return arry;
    }

}
