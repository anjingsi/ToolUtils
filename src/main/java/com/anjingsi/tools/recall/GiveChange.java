package com.anjingsi.tools.recall;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.text.DecimalFormat;
import java.util.List;

/**
 * @program: 贪心算法 （纸币找零）
 * @description 假设纸币金额为1元、5元、10元、20元、50元、100元，123元应该尽可能兑换少的纸币。
 * @author: 安静思
 * @create: 2019-12-20 14:36
 **/
@Slf4j
public class GiveChange {

    /**
     * 纸币面值
     */
    private List<Double> money = Lists.newArrayList();

    /**
     * 纸币的张数
     */
    private List<Integer> num = Lists.newArrayList();

    public GiveChange(){
        money.add(100.00);
        money.add(50.00);
        money.add(20.00);
        money.add(10.00);
        money.add(5.00);
        money.add(1.00);
        money.add(0.50);
        money.add(0.20);
        money.add(0.10);
    }

    private void calculateChange(Double amount){
        DecimalFormat df = new DecimalFormat("#.00");
        // 记录当前面值的序号
        int count = 0;
        //记录当前面值的张数
        int number = 0 ;
        while (amount >= 0.10 ){
            if(money.get(count) < amount){
                number ++ ;
                amount = Double.valueOf(df.format(amount - money.get(count)));
            }else if(money.get(count).equals(amount)){
                num.add(number+1);
                break;
            }else {
                num.add(number);
                number = 0 ;
                count ++ ;
            }
        }
    }

    public static void main(String[] args) {
        GiveChange giveChange = new GiveChange();
        giveChange.calculateChange(1028.80);
        for (int i = 0; i < giveChange.num.size(); i++) {
            if(giveChange.num.get(i) != 0){
                System.out.println("面值："+giveChange.money.get(i)+" ,张数需要："+giveChange.num.get(i));
            }
        }
    }
}
