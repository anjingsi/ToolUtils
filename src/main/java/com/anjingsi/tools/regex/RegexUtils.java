package com.anjingsi.tools.regex;

import java.util.regex.Pattern;

/**
 * 正则表达验证
 * @author: 安静思
 */
public class RegexUtils {

    /**
     * 验证Email
     *
     * @param email
     *            email地址，格式：zhangsan@zuidaima.com，zhangsan@xxx.com.cn，
     *            xxx代表邮件服务商
     * @return 验证成功返回true，验证失败返回false
     */
    public static boolean checkEmail(String email) {
        String regex = "\\w+@\\w+\\.[a-z]+(\\.[a-z]+)?";
        return Pattern.matches(regex, email);
    }

    /**
     * 验证身份证号码
     *
     * @param idCard
     *            居民身份证号码15位或18位，最后一位可能是数字或字母
     * @return 验证成功返回true，验证失败返回false
     */
    public static boolean checkIdCard(String idCard) {
        String regex = "[1-9](\\d{16}|\\d{13})[0-9xX]{1}";
        return Pattern.matches(regex, idCard);
    }

    /**
     * 验证手机号码
     * @return 验证成功返回true，验证失败返回false
     */
    public static boolean checkMobile(String mobile) {
        String regex = "^1\\d{10}$";
        return Pattern.matches(regex, mobile);
    }

    /**
     * 同时验证多个手机号码，多个号码用逗号分隔s
     * @return 验证成功返回true，验证失败返回false
     */
    public static boolean checkMultipleMobile(String mobile) {
        String regex = "^1\\d{10}(,1\\d{10})*";
        return Pattern.matches(regex, mobile);
    }




    /**
     * 验证固定电话号码
     * @return 验证成功返回true，验证失败返回false
     */
    public static boolean checkPhone(String phone) {
        String regex = "(\\+\\d+)?(\\d{3,4}\\-?)?\\d{7,8}$";
        return Pattern.matches(regex, phone);
    }
}
