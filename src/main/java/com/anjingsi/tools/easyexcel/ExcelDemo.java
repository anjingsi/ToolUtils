package com.anjingsi.tools.easyexcel;

import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.google.common.collect.Lists;

import java.io.*;
import java.util.List;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-30 11:01
 **/
public class ExcelDemo {

    private static String PATH = "C:\\Users\\Administrator\\Desktop\\test\\mail\\test.xlsx";
    public static void main(String[] args) throws IOException {
        OutputStream outputStream = new FileOutputStream(PATH);
        ExcelWriter excelWriter = EasyExcelFactory.getWriter(outputStream);
        //写仅有一个的sheet的excel文件，此场景较为通用
        Sheet sheet = new Sheet(1,0, ExcelDto.class);
        //第一个  sheet名称
        sheet.setSheetName("测试");
        //写数据到writer上下文中
        //参数含义： 创建要写入的模型数据   要写入的目标sheet
        excelWriter.write(createModeList(),sheet);
        //将上下文中的最终  outputStream 写入到指定的文件中
        excelWriter.finish();
        outputStream.close();
    }

    private static List<ExcelDto> createModeList(){
        List<ExcelDto> list = Lists.newArrayList();
        for (int i = 0; i < 100; i++) {
            ExcelDto excelDto = ExcelDto.builder().name("java"+i).age(i).password("123456").build();
            list.add(excelDto);
        }
        return list;
    }

    private static File getFile(String path) throws IOException {
        File file = new File(path);
        if(file.exists()){
            file.createNewFile();
        }
        return file;
    }
}
