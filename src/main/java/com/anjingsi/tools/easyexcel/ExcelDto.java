package com.anjingsi.tools.easyexcel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-30 11:01
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExcelDto extends BaseRowModel {

    @ExcelProperty(value = "姓名",index = 0)
    private String name;
    @ExcelProperty(value = "密码",index = 1)
    private String password;
    @ExcelProperty(value = "年龄",index = 2)
    private Integer age;
}
