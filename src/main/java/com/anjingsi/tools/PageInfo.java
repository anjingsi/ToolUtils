package com.anjingsi.tools;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @program: 分页参数信息
 * @description
 * @author: 安静思
 * @create: 2019-06-24 13:59
 **/
@Data
@ApiModel(value = "分页信息")
public class PageInfo implements Serializable{

    @ApiModelProperty(value = "每页大小,默认为10")
    @Min(value = 1,message = "每页大小不能必须大于0")
    @NotNull(message = "分页大小不能为空")
    private Integer pageSize;

    @ApiModelProperty(value = "当前页码,默认为1")
    @Min(value = 1,message = "页码必须大于0")
    @NotNull(message = "页码不能为空")
    private Integer pageNum;

    public PageInfo(){
        this.pageSize = 10 ;
        this.pageNum = 1;
    }
}
