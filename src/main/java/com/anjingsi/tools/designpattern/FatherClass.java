package com.anjingsi.tools.designpattern;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-03 16:51
 **/
public class FatherClass {
    public FatherClass() {
        System.out.println("FatherClass Create");
    }
}
