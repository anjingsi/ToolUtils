package com.anjingsi.tools.designpattern;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-03 16:52
 **/
public class ChildClass extends FatherClass {

    public ChildClass(){
        System.out.println("ChildClass Create");
    }

    public static void main(String[] args) {
        FatherClass fatherClass = new FatherClass();
        ChildClass childClass = new ChildClass();
    }
}
