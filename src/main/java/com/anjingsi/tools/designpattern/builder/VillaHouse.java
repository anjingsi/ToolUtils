package com.anjingsi.tools.designpattern.builder;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-02 09:54
 **/
public class VillaHouse extends HouseBuilder {

    private String name = "别墅";
    @Override
    public void buildBasic() {
        System.out.println(" 建造 "+name+" 打地基 ");
    }

    @Override
    public void buildWalls() {
        System.out.println(" 建造 "+name+" 砌墙 ");
    }

    @Override
    public void roofed() {
        System.out.println(" 建造 "+name+" 封顶 ");
    }
}