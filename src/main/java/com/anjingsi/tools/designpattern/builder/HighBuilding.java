package com.anjingsi.tools.designpattern.builder;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-31 17:33
 **/
public class HighBuilding extends HouseBuilder {

    private String name = "高层";
    @Override
    public void buildBasic() {
        System.out.println(" 建造 "+name+" 打地基 ");
    }

    @Override
    public void buildWalls() {
        System.out.println(" 建造 "+name+" 砌墙 ");
    }

    @Override
    public void roofed() {
        System.out.println(" 建造 "+name+" 封顶 ");
    }
}
