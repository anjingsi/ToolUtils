package com.anjingsi.tools.designpattern.builder;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-02 09:47
 **/
public class Client {

    public static void main(String[] args) {
        new HouseDirector(new CommonHouse()).constructHouse();
        new HouseDirector(new HighBuilding()).constructHouse();
        new HouseDirector(new VillaHouse()).constructHouse();
    }
}
