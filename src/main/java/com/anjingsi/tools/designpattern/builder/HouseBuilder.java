package com.anjingsi.tools.designpattern.builder;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-31 17:32
 **/
public abstract class HouseBuilder {

    private House house = new House();

    public abstract void buildBasic();

    public abstract void buildWalls();

    public abstract void roofed();

    public House buildHouse(){
        return house;
    }

}
