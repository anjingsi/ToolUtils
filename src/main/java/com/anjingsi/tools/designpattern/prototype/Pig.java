package com.anjingsi.tools.designpattern.prototype;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-31 15:42
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Pig implements Serializable,Cloneable {

    private String name;

    private int age;

    @Override
    protected Pig clone() throws CloneNotSupportedException {
        return (Pig)super.clone();
    }
}
