package com.anjingsi.tools.designpattern.prototype;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-31 14:37
 **/
public class TestDemo {

    public static void main(String[] args) throws CloneNotSupportedException {
        Sheep sheep = Sheep.builder().name("tom").age(1).color("white").build();
        Pig jack = Pig.builder().name("jack").age(2).build();
        sheep.setFriend(jack);

        System.out.println(jack.hashCode());
        for (int i = 0; i < 10; i++) {
            //方式一
//            Sheep sheep1 = new Sheep(sheep.getName(),sheep.getAge(),sheep.getColor());
//            System.out.println("拷贝第"+ (i+1)+"只羊,羊的属性为："+sheep1);

            //方式二
//            Sheep sheep1 = sheep.clone();
//            System.out.println("拷贝第"+ (i+1)+"只羊,羊的属性为："+sheep1+"，是否与第一只羊相等："+(sheep==sheep1)+",羊的朋友："+sheep1.getFriend().hashCode());

            //方式三
            Sheep sheep1 = sheep.deepClone();
            System.out.println("拷贝第"+ (i+1)+"只羊,羊的属性为："+sheep1+"，是否与第一只羊相等："+(sheep==sheep1)+",羊的朋友："+(sheep1.getFriend()==sheep.getFriend()));
        }


    }
}
