package com.anjingsi.tools.designpattern.adapter.interfaceadapter;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-02 10:21
 **/
public class Client {

    public static void main(String[] args) {

        new InterfaceAdapter(){
            @Override
            public void m1() {
                System.out.println("测试调用m1");
            }
        }.m1();
    }
}
