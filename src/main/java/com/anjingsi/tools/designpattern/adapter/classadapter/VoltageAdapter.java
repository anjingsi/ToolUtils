package com.anjingsi.tools.designpattern.adapter.classadapter;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-02 10:00
 **/
public class VoltageAdapter extends Voltage220V implements IVoltage5V {
    @Override
    public int output5V() {
        System.out.println("转换后的电压为："+ (output220V() / 44));
        return output220V() / 44;
    }
}
