package com.anjingsi.tools.designpattern.adapter.objectadapter;

import com.anjingsi.tools.designpattern.adapter.classadapter.Phone;
import com.anjingsi.tools.designpattern.adapter.classadapter.Voltage220V;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-02 10:01
 **/
public class Client {
    public static void main(String[] args) {
        new Phone().charging(new VoltageAdapter(new Voltage220V()));
    }
}
