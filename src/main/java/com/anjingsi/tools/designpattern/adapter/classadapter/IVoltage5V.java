package com.anjingsi.tools.designpattern.adapter.classadapter;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-02 09:58
 **/
public interface IVoltage5V {

    int output5V();
}
