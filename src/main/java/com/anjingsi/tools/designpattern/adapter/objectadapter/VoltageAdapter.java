package com.anjingsi.tools.designpattern.adapter.objectadapter;

import com.anjingsi.tools.designpattern.adapter.classadapter.IVoltage5V;
import com.anjingsi.tools.designpattern.adapter.classadapter.Voltage220V;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-02 10:00
 **/
public class VoltageAdapter implements IVoltage5V {
    private Voltage220V voltage220V;

    public VoltageAdapter(Voltage220V voltage220V){
        this.voltage220V = voltage220V ;
    }
    @Override
    public int output5V() {
        System.out.println("转换后的电压为："+ (voltage220V.output220V() / 44));
        return voltage220V.output220V() / 44;
    }

    public void setVoltage220V(Voltage220V voltage220V){
        this.voltage220V = voltage220V ;
    }
}
