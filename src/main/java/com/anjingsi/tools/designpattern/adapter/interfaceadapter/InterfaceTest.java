package com.anjingsi.tools.designpattern.adapter.interfaceadapter;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-02 10:20
 **/
public interface InterfaceTest {
    void m1();
    void m2();
    void m3();
}
