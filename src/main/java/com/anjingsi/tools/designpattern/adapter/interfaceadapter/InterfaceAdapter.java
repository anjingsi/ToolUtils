package com.anjingsi.tools.designpattern.adapter.interfaceadapter;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-02 10:21
 **/
public abstract class InterfaceAdapter implements InterfaceTest {
    public InterfaceAdapter(){}

    @Override
    public void m1() {

    }

    @Override
    public void m2() {

    }

    @Override
    public void m3() {

    }
}
