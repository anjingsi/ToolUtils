package com.anjingsi.tools.designpattern.decorator;

public class Milk extends Decorator {
    public Milk(Drink obj) {
        super(obj);
        this.setDescribe(" 牛奶 ");
        this.setPrice(2.0F);
    }
}