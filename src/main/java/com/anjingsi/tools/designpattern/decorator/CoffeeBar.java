package com.anjingsi.tools.designpattern.decorator;

public class CoffeeBar {
    public CoffeeBar() {
    }

    public static void main(String[] args) {
        Drink order = new LongBlack();
        System.out.println("费用1=" + order.cost() +"  描述=" + order.getDescribe());
        Drink milk = new Milk(order);
        System.out.println("order 加入一份牛奶 费用 =" + milk.cost() + "  order 加入一份牛奶 描述 = " + milk.getDescribe());
        Drink chocolate = new Chocolate(milk);
        System.out.println("order 加入一份牛奶 加入一份巧克力  费用 =" + chocolate.cost()+"  order 加入一份牛奶 加入一份巧克力 描述 = " + chocolate.getDescribe());
        order = new Chocolate(order);
        System.out.println("order 加入一份牛奶 加入2份巧克力   费用 =" + order.cost());
        System.out.println("order 加入一份牛奶 加入2份巧克力 描述 = " + order.getDescribe());
        System.out.println("===========================");
        Drink order3 = new DeCaf();
        System.out.println("order2 无因咖啡  费用 =" + order3.cost());
        System.out.println("order2 无因咖啡 描述 = " + order3.getDescribe());
        Drink order4 = new Milk(order3);
        System.out.println("order2 无因咖啡 加入一份牛奶  费用 =" + order4.cost());
        System.out.println("order2 无因咖啡 加入一份牛奶 描述 = " + order4.getDescribe());
    }
}