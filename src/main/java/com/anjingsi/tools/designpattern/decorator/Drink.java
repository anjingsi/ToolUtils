package com.anjingsi.tools.designpattern.decorator;

import lombok.Data;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-02 15:27
 **/
@Data
public abstract class Drink {

    private String describe;

    private float price = 0.0F;

    public abstract float cost();

}
