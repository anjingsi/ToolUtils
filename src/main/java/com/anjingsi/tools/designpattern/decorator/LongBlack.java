package com.anjingsi.tools.designpattern.decorator;

public class LongBlack extends Coffee {
    public LongBlack() {
        this.setDescribe(" longblack ");
        this.setPrice(12.0F);
    }
}
