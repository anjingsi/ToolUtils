package com.anjingsi.tools.designpattern.decorator;

public class Chocolate extends Decorator {
    public Chocolate(Drink obj) {
        super(obj);
        this.setDescribe(" 巧克力 ");
        this.setPrice(3.0F);
    }
}