package com.anjingsi.tools.designpattern.decorator;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-02 15:28
 **/
public class Coffee extends Drink {

    public Coffee(){}

    @Override
    public float cost() {
        return super.getPrice();
    }
}
