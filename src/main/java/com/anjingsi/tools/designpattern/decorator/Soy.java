package com.anjingsi.tools.designpattern.decorator;

public class Soy extends Decorator {
    public Soy(Drink obj) {
        super(obj);
        this.setDescribe(" 豆浆  ");
        this.setPrice(1.5F);
    }
}