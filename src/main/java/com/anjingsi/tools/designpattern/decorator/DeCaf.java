package com.anjingsi.tools.designpattern.decorator;

public class DeCaf extends Coffee {
    public DeCaf() {
        this.setDescribe(" 无因咖啡 ");
        this.setPrice(10.0F);
    }
}
