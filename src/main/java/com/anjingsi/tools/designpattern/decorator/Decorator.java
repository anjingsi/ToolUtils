package com.anjingsi.tools.designpattern.decorator;

public class Decorator extends Drink {
    private Drink drink;

    public Decorator(Drink obj) {
        this.drink = obj;
    }

    @Override
    public float cost() {
        return super.getPrice() + this.drink.cost();
    }

    @Override
    public String getDescribe() {
        return this.drink + " " + this.getPrice() + " && " + this.drink.getDescribe();
    }
}
