package com.anjingsi.tools.designpattern.decorator;

public class Espresso extends Coffee {
    public Espresso() {
        this.setDescribe(" 意大利咖啡 ");
        this.setPrice(11.0F);
    }
}