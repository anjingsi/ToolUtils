package com.anjingsi.tools.designpattern.factory.simple;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-31 10:09
 **/
public class InputUtils {

    // 写一个方法，可以获取客户希望订购的披萨种类
    public static String getType() {
        try {
            System.out.println("输入 pizza 种类:");
            BufferedReader strin = new BufferedReader(new InputStreamReader(System.in));
            String str = strin.readLine();
            return str;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
