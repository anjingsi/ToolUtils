package com.anjingsi.tools.designpattern.factory.simple;

import com.anjingsi.tools.designpattern.factory.BJCheesePizza;
import com.anjingsi.tools.designpattern.factory.BJPepperPizza;
import com.anjingsi.tools.designpattern.factory.GreekPizza;
import com.anjingsi.tools.designpattern.factory.Pizza;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-31 10:17
 **/
public class SimplePizzaOrder {

    public static Pizza getPizza(String orderType){
        Pizza pizza = null;
        if (orderType.equals("bjcheese")) {
            pizza = new BJCheesePizza();
        } else if (orderType.equals("bjpepper")) {
            pizza = new BJPepperPizza();
        }else if (orderType.equals("greek")) {
            pizza = new GreekPizza();
        }
        return pizza;
    }

}
