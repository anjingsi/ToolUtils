package com.anjingsi.tools.designpattern.factory.method;

import com.anjingsi.tools.designpattern.factory.Pizza;
import com.anjingsi.tools.designpattern.factory.simple.SimplePizzaOrder;

import static com.anjingsi.tools.designpattern.factory.simple.InputUtils.getType;

/**
 * @program: ToolUtils
 * @description 简单工厂实现
 * @author: 安静思
 * @create: 2019-12-31 10:09
 **/
public abstract class AbsOrderPizza {

    Pizza pizza = null;
    String orderType = "";

    public abstract Pizza createPizza(String orderType);

    // 构造器
    public AbsOrderPizza() {
        do {
            orderType = getType();
            pizza = createPizza(orderType);
            // 输出pizza
            if (pizza != null) { // 订购成功
                pizza.prepare();
                pizza.bake();
                pizza.cut();
                pizza.box();
            } else {
                System.out.println(" 订购披萨失败 ");
                break;
            }
        } while (true);
    }


}
