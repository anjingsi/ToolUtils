package com.anjingsi.tools.designpattern.factory.method;


import com.anjingsi.tools.designpattern.factory.BJCheesePizza;
import com.anjingsi.tools.designpattern.factory.BJPepperPizza;
import com.anjingsi.tools.designpattern.factory.Pizza;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-31 11:09
 **/
public class BJPizzaAbsOrder extends AbsOrderPizza {

    @Override
    public Pizza createPizza(String orderType) {
        Pizza pizza = null;
        if (orderType.equals("cheese")) {
            pizza = new BJCheesePizza();
        } else if (orderType.equals("pepper")) {
            pizza = new BJPepperPizza();
        }
        return pizza;
    }
}
