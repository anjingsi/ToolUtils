package com.anjingsi.tools.designpattern.factory;

public class LDPepperPizza extends Pizza{

	@Override
	public void prepare() {
		// TODO Auto-generated method stub
		setName("伦敦的胡椒pizza");
		System.out.println("伦敦的胡椒pizza 准备原材料");
	}

	@Override
	public void makingProcess() {
		prepare();
		bake();
		cut();
		box();
	}
}
