package com.anjingsi.tools.designpattern.factory.abs;

import com.anjingsi.tools.designpattern.factory.Pizza;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-31 11:21
 **/
public interface AbsFactory {

    Pizza createPizza(String orderType);
}
