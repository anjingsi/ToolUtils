package com.anjingsi.tools.designpattern.factory.abs;

import com.anjingsi.tools.designpattern.factory.Pizza;

import static com.anjingsi.tools.designpattern.factory.simple.InputUtils.getType;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-31 11:24
 **/
public class OrderPizz2 {

    public OrderPizz2(AbsFactory absFactory){
        String orderType = "";
        Pizza pizza = null;
        do {
            orderType = getType();
            pizza = absFactory.createPizza(orderType);
            // 输出pizza
            if (pizza != null) { // 订购成功
                pizza.prepare();
                pizza.bake();
                pizza.cut();
                pizza.box();
            } else {
                System.out.println(" 订购披萨失败 ");
                break;
            }
        } while (true);
    }
}
