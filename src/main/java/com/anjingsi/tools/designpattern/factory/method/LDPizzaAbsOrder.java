package com.anjingsi.tools.designpattern.factory.method;


import com.anjingsi.tools.designpattern.factory.*;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-31 11:09
 **/
public class LDPizzaAbsOrder extends AbsOrderPizza {

    @Override
    public Pizza createPizza(String orderType) {
        Pizza pizza = null;
        if (orderType.equals("cheese")) {
            pizza = new LDCheesePizza();
        } else if (orderType.equals("pepper")) {
            pizza = new LDPepperPizza();
        }
        return pizza;
    }
}
