package com.anjingsi.tools.designpattern.factory;

public class GreekPizza extends Pizza {

	@Override
	public void prepare() {
		// TODO Auto-generated method stub
		setName("希腊披萨");
		System.out.println("给希腊披萨 准备原材料 ");
	}

	@Override
	public void makingProcess() {
		prepare();
		bake();
		cut();
		box();
	}

}
