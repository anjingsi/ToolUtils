package com.anjingsi.tools.designpattern.factory;

/**
 * @program: ToolUtils
 * @description  披萨
 * @author: 安静思
 * @create: 2019-12-31 09:55
 **/
public abstract class Pizza {

    private String name;

    public Pizza(String name){
        this.name = name;
    }

    public Pizza(){}

    public abstract void prepare();

    public void bake() {
        System.out.println(name + " baking;");
    }

    public void cut() {
        System.out.println(name + " cutting;");
    }

    //打包
    public void box() {
        System.out.println(name + " boxing;");
    }

    /**
     * 披萨的制作过程
     */
    public void makingProcess() {
        prepare();
        bake();
        cut();
        box();
    }

    public void setName(String name) {
        this.name = name;
    }

}
