package com.anjingsi.tools.designpattern.factory.abs;

import com.anjingsi.tools.designpattern.factory.BJCheesePizza;
import com.anjingsi.tools.designpattern.factory.BJPepperPizza;
import com.anjingsi.tools.designpattern.factory.Pizza;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-31 11:22
 **/
public class BJFactory implements AbsFactory{

    @Override
    public Pizza createPizza(String orderType) {
        Pizza pizza = null;
        if (orderType.equals("cheese")) {
            pizza = new BJCheesePizza();
        } else if (orderType.equals("pepper")) {
            pizza = new BJPepperPizza();
        }
        return pizza;
    }
}
