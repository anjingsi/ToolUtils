package com.anjingsi.tools.designpattern.factory.simple;

import com.anjingsi.tools.designpattern.factory.BJCheesePizza;
import com.anjingsi.tools.designpattern.factory.BJPepperPizza;
import com.anjingsi.tools.designpattern.factory.Pizza;

import static com.anjingsi.tools.designpattern.factory.simple.InputUtils.getType;

/**
 * @program: ToolUtils
 * @description  普通的方法
 * @author: 安静思
 * @create: 2019-12-31 10:09
 **/
public class OrderPizza {
//     构造器
	public OrderPizza() {
		Pizza pizza = null;
		String orderType; //披萨的类型
		do {
			orderType = getType();
			if (orderType.equals("bjcheese")) {
				pizza = new BJCheesePizza();
			} else if (orderType.equals("bjpepper")) {
				pizza = new BJPepperPizza();
			}  else {
				break;
			}
            pizza.makingProcess();
		} while (true);
	}

}
