package com.anjingsi.tools.designpattern.factory;

public class LDCheesePizza extends Pizza{

	@Override
	public void prepare() {
		// TODO Auto-generated method stub
		setName("伦敦的奶酪pizza");
		System.out.println(" 伦敦的奶酪pizza 准备原材料");
	}

	@Override
	public void makingProcess() {
		prepare();
		bake();
		cut();
		box();
	}
}
