package com.anjingsi.tools.designpattern.factory;

import com.anjingsi.tools.designpattern.factory.abs.BJFactory;
import com.anjingsi.tools.designpattern.factory.abs.LDFactory;
import com.anjingsi.tools.designpattern.factory.abs.OrderPizz2;
import com.anjingsi.tools.designpattern.factory.method.AbsOrderPizza;

import java.util.Calendar;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-31 10:12
 **/
public class TestDemo {
    public static void main(String[] args) {
//        new OrderPizza();
//        new OrderPizza1();
//        new BJPizzaAbsOrder();
//        new LDPizzaAbsOrder();

//        new OrderPizz2(new BJFactory());
        new OrderPizz2(new LDFactory());
        Calendar calendar = Calendar.getInstance();
    }
}
