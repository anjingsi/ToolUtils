package com.anjingsi.tools.designpattern.bridge;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-02 11:41
 **/
public class Vivo implements Brand {

    @Override
    public void call() {
        System.out.println("vivo 手机打电话");
    }

    @Override
    public void open() {
        System.out.println("vivo 开机");
    }

    @Override
    public void close() {
        System.out.println("vivo 关机");
    }
}
