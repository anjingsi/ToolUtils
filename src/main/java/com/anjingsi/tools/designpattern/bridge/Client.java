package com.anjingsi.tools.designpattern.bridge;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-02 11:44
 **/
public class Client {
    public static void main(String[] args) {
        Phone phone = new FolderPhone(new XiaoMi());
        phone.open();
        phone.call();
        phone.close();

        Phone phone1 = new FolderPhone(new Vivo());
        phone1.open();
        phone1.call();
        phone1.close();
    }
}
