package com.anjingsi.tools.designpattern.bridge;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-02 11:43
 **/
public class UpRightPhone extends Phone {

    public UpRightPhone(Brand brand) {
        super(brand);
    }

    @Override
    public void open(){
        super.open();
        System.out.println("直立手机   开机了");
    }

    @Override
    public void close(){
        super.close();
        System.out.println("直立手机   关机了");
    }

    @Override
    public void call(){
        super.call();
        System.out.println("直立手机   打电话");
    }
}
