package com.anjingsi.tools.designpattern.bridge;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-02 11:41
 **/
public interface Brand {

    void call();

    void open();

    void close();
}
