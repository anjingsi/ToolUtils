package com.anjingsi.tools.designpattern.bridge;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2020-01-02 11:42
 **/
public class XiaoMi implements Brand {

    @Override
    public void call() {
        System.out.println("小米 手机打电话");
    }

    @Override
    public void open() {
        System.out.println("小米 开机");
    }

    @Override
    public void close() {
        System.out.println("小米 关机");
    }
}