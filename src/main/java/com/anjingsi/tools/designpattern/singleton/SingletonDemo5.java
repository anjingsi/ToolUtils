package com.anjingsi.tools.designpattern.singleton;

/**
 * @program: ToolUtils
 * @description 双重检查
 * @author: 安静思
 * @create: 2019-12-30 17:12
 **/
public class SingletonDemo5 {

    private SingletonDemo5(){}

    private static SingletonDemo5 instance;

    private static SingletonDemo5 getSingletonDemo(){
        if(instance == null){
            synchronized (SingletonDemo5.class){
                if(instance == null){
                    instance = new SingletonDemo5();
                }
            }
        }
        return instance;
    }

    public static SingletonDemo5 getInstance(){
        return getSingletonDemo();
    }
}
