package com.anjingsi.tools.designpattern.singleton;

/**
 * @program: ToolUtils
 * @description 单例模式懒汉式
 * @author: 安静思
 * @create: 2019-12-30 17:12
 **/
public class SingletonDemo4 {

    private SingletonDemo4(){}

    private static SingletonDemo4 instance;

    private static synchronized SingletonDemo4 getSingletonDemo(){
        if(instance == null){
            instance = new SingletonDemo4();
        }
        return instance;
    }

    public static SingletonDemo4 getInstance(){
        return getSingletonDemo();
    }
}
