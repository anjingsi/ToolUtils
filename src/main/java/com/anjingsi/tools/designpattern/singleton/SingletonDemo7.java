package com.anjingsi.tools.designpattern.singleton;

/**
 * @program: ToolUtils
 * @description 静态内部类
 * @author: 安静思
 * @create: 2019-12-30 17:12
 **/
public enum  SingletonDemo7 {

    INSTANCE;

    public void sayHello(){
        System.out.println("~~~~~say hello~~~~~~");
    }
}
