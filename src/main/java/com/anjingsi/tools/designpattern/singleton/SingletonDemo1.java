package com.anjingsi.tools.designpattern.singleton;

/**
 * @program: ToolUtils
 * @description 单例模式饿汉式
 * @author: 安静思
 * @create: 2019-12-30 17:12
 **/
public class SingletonDemo1 {

    private SingletonDemo1(){}

    private final static SingletonDemo1 instance = new SingletonDemo1();

    public static SingletonDemo1 getInstance(){
        return instance;
    }
}
