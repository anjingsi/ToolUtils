package com.anjingsi.tools.designpattern.singleton;

/**
 * @program: ToolUtils
 * @description 单例模式懒汉式
 * @author: 安静思
 * @create: 2019-12-30 17:12
 **/
public class SingletonDemo3 {

    private SingletonDemo3(){}

    private static SingletonDemo3 instance;

    private static SingletonDemo3 getSingletonDemo(){
        if(instance == null){
            instance = new SingletonDemo3();
        }
        return instance;
    }

    public static SingletonDemo3 getInstance(){
        return getSingletonDemo();
    }
}
