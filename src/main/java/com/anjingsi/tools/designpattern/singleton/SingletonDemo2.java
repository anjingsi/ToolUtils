package com.anjingsi.tools.designpattern.singleton;

/**
 * @program: ToolUtils
 * @description 单例模式饿汉式
 * @author: 安静思
 * @create: 2019-12-30 17:12
 **/
public class SingletonDemo2 {

    private SingletonDemo2(){}

    private static SingletonDemo2 instance;

    static {
        instance = new SingletonDemo2();
    }

    public static SingletonDemo2 getInstance(){
        return instance;
    }
}
