package com.anjingsi.tools.designpattern.singleton;

/**
 * @program: ToolUtils
 * @description 静态内部类
 * @author: 安静思
 * @create: 2019-12-30 17:12
 **/
public class SingletonDemo6 {

    private SingletonDemo6(){}

    private static class SingletonInstance{
        public final static SingletonDemo6 instance = new SingletonDemo6();
    }

    public static SingletonDemo6 getInstance(){
        return SingletonInstance.instance;
    }
}
