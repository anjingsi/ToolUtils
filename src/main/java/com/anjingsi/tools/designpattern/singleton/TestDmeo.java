package com.anjingsi.tools.designpattern.singleton;

/**
 * @program: ToolUtils
 * @description
 * @author: 安静思
 * @create: 2019-12-30 17:17
 **/
public class TestDmeo {
    public static void main(String[] args) {
        //静态内部类
        SingletonDemo7 singletonDemo1 = SingletonDemo7.INSTANCE;
        SingletonDemo7 singletonDemo2 = SingletonDemo7.INSTANCE;
        System.out.println(singletonDemo1 == singletonDemo2);
        System.out.println(singletonDemo1.hashCode() +"=="+ singletonDemo2.hashCode());

//        //静态内部类
//        SingletonDemo6 singletonDemo1 = SingletonDemo6.getInstance();
//        SingletonDemo6 singletonDemo2 = SingletonDemo6.getInstance();
//        System.out.println(singletonDemo1 == singletonDemo2);
//        System.out.println(singletonDemo1.hashCode() +"=="+ singletonDemo2.hashCode());

//        //双重检查
//        SingletonDemo5 singletonDemo1 = SingletonDemo5.getInstance();
//        SingletonDemo5 singletonDemo2 = SingletonDemo5.getInstance();
//        System.out.println(singletonDemo1 == singletonDemo2);
//        System.out.println(singletonDemo1.hashCode() +"=="+ singletonDemo2.hashCode());

//        //懒汉式
//        SingletonDemo4 singletonDemo1 = SingletonDemo4.getInstance();
//        SingletonDemo4 singletonDemo2 = SingletonDemo4.getInstance();
//        System.out.println(singletonDemo1 == singletonDemo2);
//        System.out.println(singletonDemo1.hashCode() +"=="+ singletonDemo2.hashCode());

//        //懒汉式
//        SingletonDemo3 singletonDemo1 = SingletonDemo3.getInstance();
//        SingletonDemo3 singletonDemo2 = SingletonDemo3.getInstance();
//        System.out.println(singletonDemo1 == singletonDemo2);
//        System.out.println(singletonDemo1.hashCode() +"=="+ singletonDemo2.hashCode());

//        //饿汉式1
//        SingletonDemo1 singletonDemo = SingletonDemo1.getInstance();
//        SingletonDemo1 singletonDemo11 = SingletonDemo1.getInstance();
//        System.out.println(singletonDemo == singletonDemo11);
//        System.out.println(singletonDemo.hashCode() == singletonDemo11.hashCode());

//        //饿汉式2
//        SingletonDemo2 singletonDemo2 = SingletonDemo2.getInstance();
//        SingletonDemo2 singletonDemo3 = SingletonDemo2.getInstance();
//        System.out.println(singletonDemo2 == singletonDemo3);
//        System.out.println(singletonDemo2.hashCode() == singletonDemo3.hashCode());
    }
}
