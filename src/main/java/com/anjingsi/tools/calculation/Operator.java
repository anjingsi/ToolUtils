package com.anjingsi.tools.calculation;

/**
 * 运算符
 */
public enum Operator {
    PLUS("+", 1) {
        @Override
        public double compute(double num1, double num2) {
            return num1 + num2;
        }
    },
    MINUS("-", 1) {
        @Override
        public double compute(double num1, double num2) {
            return num1 - num2;
        }
    },
    MULTIPLY("*", 2) {
        @Override
        public double compute(double num1, double num2) {
            return num1 * num2;
        }
    },
    DIVIDE("/", 2) {
        @Override
        public double compute(double num1, double num2) {
            return num1 / num2;
        }
    },
    REMAINDER("%", 2) {
        @Override
        public double compute(double num1, double num2) {
            return num1 % num2;
        }
    },
    EXPONENTIATION("^", 3) {
        @Override
        public double compute(double num1, double num2) {
            return Math.pow(num1, num2);
        }
    },
    INTEGER_DIVISION("//", 2) {
        @Override
        public double compute(double num1, double num2) {
            return Math.floor(num1 / num2);
        }
    },
    RIGHT_BRACKET(")", 0) {
        @Override
        public double compute(double num1, double num2) {
            throw new UnsupportedOperationException();
        }
    },
    LEFT_BRACKET("(", 0) {
        @Override
        public double compute(double num1, double num2) {
            throw new UnsupportedOperationException();
        }
    },
    OTHER("", 0) {
        @Override
        public double compute(double num1, double num2) {
            return 0;
        }
    };

    /**
     * 符号的值
     */
    private String value;
    /**
     * 优先级
     */
    private int priority;

    private Operator(String value, int priority) {
        this.value = value;
        this.priority = priority;
    }

    public static Operator getOperator(String value) {
        for (Operator operator : Operator.values()) {
            if (operator.value.equals(value)) {
                return operator;
            }
        }
        return OTHER;
    }

    /**
     * 是否为操作符
     * @return
     */
    public static boolean isOperator(String value){
        for(Operator operator : Operator.values()){
            if(operator.value.equals(value)){
                return true;
            }
        }
        return false;
    }

    public int getPriority() {
        return priority;
    }

    public String getValue() {
        return value;
    }

    public abstract double compute(double num1, double num2);
}