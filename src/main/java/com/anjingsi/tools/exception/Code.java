package com.anjingsi.tools.exception;

public class Code {
    int code1;
    int code2;
    int code3;

    public Code(int code1, int code2, int code3) {
        this.code1 = code1;
        this.code2 = code2;
        this.code3 = code3;
    }

    public int getCode1() {
        return this.code1;
    }

    public int getCode2() {
        return this.code2;
    }

    public int getCode3() {
        return this.code3;
    }

    public void setCode1(int code1) {
        this.code1 = code1;
    }

    public void setCode2(int code2) {
        this.code2 = code2;
    }

    public void setCode3(int code3) {
        this.code3 = code3;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof Code)) {
            return false;
        } else {
            Code other = (Code)o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.getCode1() != other.getCode1()) {
                return false;
            } else if (this.getCode2() != other.getCode2()) {
                return false;
            } else {
                return this.getCode3() == other.getCode3();
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof Code;
    }

    public int hashCode() {
        int result = 1;
        result = result * 59 + this.getCode1();
        result = result * 59 + this.getCode2();
        result = result * 59 + this.getCode3();
        return result;
    }

    public String toString() {
        return "Code(code1=" + this.getCode1() + ", code2=" + this.getCode2() + ", code3=" + this.getCode3() + ")";
    }
}
