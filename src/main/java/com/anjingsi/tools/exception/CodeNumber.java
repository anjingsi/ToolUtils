package com.anjingsi.tools.exception;


/**
 * @author anyp
 * @date 2018/6/6
 */
public class CodeNumber {
    //    应用异常码
    public static final int CODE = 0x001;

    //接口信息
    public static final int INTERFACE_INFO = 0x01;
    public static final Code INTERFACE_NAME_ISNULL = new Code(CODE, INTERFACE_INFO, 0x01);//名字不能为空

    //接口访问信息
    public static final int INTERFACE_CALL_INFO = 0x02;
    public static final Code PARAMETER_ISNULL = new Code(CODE, INTERFACE_CALL_INFO, 0x01);//参数错误

}
