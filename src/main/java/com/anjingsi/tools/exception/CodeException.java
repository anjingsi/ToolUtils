package com.anjingsi.tools.exception;

public interface CodeException {

    String getCode();

    String getMessage();
}
